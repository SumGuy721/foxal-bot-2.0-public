import sqlite3
import sys
from datetime import datetime

con = sqlite3.connect("modules/Scheduling.db")
cur = con.cursor()


def setup_sc_db():
    cur.execute("CREATE TABLE IF NOT EXISTS SCHEDULE(date date, user_id int, jobtype text)")
    con.commit()


async def add_job(user_id, date, job_type):
    """
    Adds a job to the scheduler.
    Possible job_types are Fast initiation, Normal initiation, Slow initiation
    """
    query = "INSERT INTO SCHEDULE VALUES(?,?,?)"
    cur.execute(query, [date, user_id, job_type])
    con.commit()


# noinspection PyBroadException
def get_due_jobs():
    """
    Get due jobs for today
    """
    query = "SELECT STRFTIME('%Y/%m/%d', date), user_id, jobtype FROM SCHEDULE WHERE STRFTIME('%Y/%m/%d', date) = ?"
    try:
        return con.execute(query, [datetime.today().strftime("%Y/%m/%d")]).fetchall()
    except:
        print("Error in get_due_jobs: ", sys.exc_info()[0], ": ", sys.exc_info()[1])


def view_test():
    query = "CREATE VIEW IF NOT EXISTS viewtest AS SELECT STRFTIME('%Y/%m/%d', date), user_id, jobtype FROM SCHEDULE"
    con.execute(query)
    test = con.execute("SELECT * FROM viewtest").fetchall()
    con.execute("DROP VIEW viewtest")
    return test
