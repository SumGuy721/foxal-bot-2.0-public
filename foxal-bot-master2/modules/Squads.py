import sqlite3

con = sqlite3.connect("modules/Squads.db")
cur = con.cursor()


def setup_sq_db():
    cur.execute("CREATE TABLE IF NOT EXISTS SQUADCOUNT(user_id int, squadtype text, squadcount int)")
    con.commit()


async def add_sq_count(user_id, squadtype):
    """
    Adds a squadder to the database and sets to/increases count by 1
    user_id = int
    squadtype = str(Create or Join)
    """
    # Check if user is already in db
    check_query = "SELECT user_id FROM SQUADCOUNT WHERE user_id = ? AND squadtype = ?"
    # Add to count if yes
    if cur.execute(check_query, [user_id, squadtype]).fetchone():
        cur.execute("""UPDATE SQUADCOUNT SET squadcount = squadcount + 1
                            WHERE user_id = ? AND squadtype = ?""", [user_id, squadtype])
        con.commit()
    # Create if not
    else:
        cur.execute("INSERT INTO SQUADCOUNT VALUES (?,?,?)", [user_id, squadtype, 1])
        con.commit()


def get_gold_names():
    """
Creates a list of member id's for the highest counting members within database
returns a list, [0] = Create; [1] = Join
    """
    results = list()
    for x in ["Create", "Join"]:
        query = """SELECT user_id, squadcount FROM SQUADCOUNT WHERE squadtype = ? """
        result = con.execute(query, [x]).fetchall()
        iter_squad_count = 0
        role_members = list()
        for res in result:
            # print(res[1], iter_squad_count)
            if res[1] > iter_squad_count:
                iter_squad_count = res[1]
                role_members = list()
                role_members.append(res[0])
            elif res[1] == iter_squad_count:
                role_members.append(res[0])
            else:
                pass
        results.append(role_members)
    return results


def clear_gold_names_db():
    # Clear the database
    cur.execute("DELETE FROM SQUADCOUNT")
    con.commit()
