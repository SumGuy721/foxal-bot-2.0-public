def get_visible_name(bot, server_id, user_id):
    guild = bot.get_guild(server_id)
    if guild.get_member(user_id).nick:
        return guild.get_member(user_id).nick
    else:
        return guild.get_member(user_id).name


async def error_message_sender(bot, bot_log_channel_id, error_message):
    await bot.get_channel(bot_log_channel_id).send(f"An error occurred in: {error_message}")
