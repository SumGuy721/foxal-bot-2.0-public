from .RoleHandler import user_has_role

import sqlite3

initiate_con = sqlite3.connect("modules/Initiation.db")
initiate_cur = initiate_con.cursor()


def setup_init_db():
    initiate_cur.execute("CREATE TABLE IF NOT EXISTS initiationmessages(user_id int, normalcount int, squadcount int)")
    initiate_cur.execute("CREATE TABLE IF NOT EXISTS fails(user_id int)")
    initiate_con.commit()


def add_init_count(user_id, msg_or_squad, bot, server_id, initiate_role_name, channel="", meet_channel_id=""):
    create_query = "INSERT INTO initiationmessages VALUES (?,?,?)"
    check_query = "SELECT user_id FROM initiationmessages WHERE user_id = ?"
    if user_has_role(initiate_role_name, user_id, bot, server_id):
        if msg_or_squad == "Squad":
            # Check if user is already in db
            # Add to count if yes
            if initiate_cur.execute(check_query, [user_id]).fetchone():
                msg_type = "squad"
                initiate_cur.execute(f"UPDATE initiationmessages SET {msg_type}count = {msg_type}count + 1 WHERE"
                                     f" user_id = ?", [user_id])
                initiate_con.commit()
            # Create if not
            else:
                initiate_cur.execute(create_query, [user_id, 1, 0])
        elif msg_or_squad == "Message":
            guild = bot.get_guild(server_id)
            meet_channel = guild.get_channel(meet_channel_id)
            if channel == meet_channel:
                pass
            else:
                # Check if user is already in db
                # Add to count if yes
                if initiate_cur.execute(check_query, [user_id]).fetchone():
                    msg_type = "normal"
                    initiate_cur.execute(f"UPDATE initiationmessages SET {msg_type}count = {msg_type}count + 1 WHERE"
                                         f" user_id = ?", [user_id])
                    initiate_con.commit()
                # Create if not
                else:
                    initiate_cur.execute(create_query, [user_id, 1, 0])
    initiate_con.commit()


def get_init_count(user_id):
    query = "SELECT normalcount, squadcount FROM initiationmessages WHERE user_id = ?"
    return initiate_cur.execute(query, [user_id]).fetchone()


def remove_init(user_id):
    query = "DELETE FROM initiationmessages WHERE user_id = ?"
    initiate_cur.execute(query, [user_id])
    initiate_con.commit()


async def init_check_message(visible_name, init_channel, msg_counts, bot_channel, check_type, pass_or_fail):
    if pass_or_fail == "Pass":
        await init_channel.send(f"{visible_name} has passed their initiation,"
                                f" please add their roles with /game_member."
                                f"\nStats: squads = {msg_counts[1]}; messages"
                                f" (excl. meet & squads): {msg_counts[0]}")
    if pass_or_fail == "Fail":
        if check_type == "Slow":
            await init_channel.send(f"{visible_name} has failed the last check."
                                    f"\nStats: squads = {msg_counts[1]}; total messages"
                                    f" (excl. meet & squads): {msg_counts[0]}")
        else:
            await bot_channel.send(f"{visible_name} has failed the {check_type} check."
                                   f"\nStats: squads = {msg_counts[1]}; messages"
                                   f" (excl. meet & squads): {msg_counts[0]}")


def add_fail_db(user_id):
    try:
        create_query = "INSERT INTO fails VALUES (?)"
        initiate_cur.execute(create_query, [user_id])
        initiate_con.commit()
    except:
        print("Failed in adding initiate to fail db.")


def fetch_fail_db():
    try:
        initiate_cur.execute("SELECT user_id FROM fails").fetchall()
        initiate_con.commit()
    except:
        print("Failed in fetching initiate from fail db.")


def clear_fail_db():
    try:
        initiate_cur.execute("DELETE FROM fails")
        initiate_con.commit()
    except:
        print("Failed in clearing fail db.")
