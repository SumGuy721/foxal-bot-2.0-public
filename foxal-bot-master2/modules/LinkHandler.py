import re
import requests
from enum import Enum
from urllib.error import URLError
from urllib.request import urlopen

from discord import DiscordException

LOWERCASE_WORDS = ['of', ]
UPPERCASE_WORDS = ['MK1']


# noinspection PyUnusedLocal
def check_if_url_valid(query_url=""):
    """
    Run a few cursory checks on the passed URL
    """
    # Check if the page exists
    try:
        urlopen(query_url)
    except URLError as e:
        name = query_url[query_url.rfind('/') + 1:]
        query_url = name + ": " + str(e.reason)
    except Exception as e:
        query_url = "BAD URL: " + str(e)


async def link_handler(ctx, link_type, args):
    try:
        if link_type == LinkType.WIKI:
            wiki_url = "https://warframe.fandom.com/wiki/"
            search_string = await string_prep(args)
            check_if_url_valid(search_string)
            query_url = wiki_url + search_string
            await ctx.respond(query_url)
        else:
            wiki_url = "https://warframe.market/items/"
            search_string = args.strip().lower().replace(' ', '_')
            search_string = re.sub(r"(_)\1+", '_', search_string)
            check_if_url_valid(search_string)
            query_url = wiki_url + search_string
            await ctx.respond(query_url)
    except DiscordException:
        ctx.send("Something went wrong in finding the link")


async def string_prep(search_string):
    # Replaces spaces and removes extra underscores
    search_string = search_string.title().strip().replace(' ', '_')
    search_string = re.sub(r"(_)\1+", '_', search_string)

    # lower case words
    for word in LOWERCASE_WORDS:
        word_in = '_' + word.title() + '_'
        word_out = '_' + word.lower() + '_'
        search_string = search_string.replace(word_in, word_out)

    # uppercase words. Support for initial words only as no other case exists
    for word in UPPERCASE_WORDS:
        wl = word.lower()
        ssl = search_string.lower()
        if wl not in ssl:
            continue
        # capitalize whatever comes after UPPERCASE_WORD
        search_string = re.sub(wl + "(.*)", lambda x: word + x.group(1).title(), ssl)

    # capitalize words in parentheses
    search_string = re.sub("\\((.*)\\)", lambda x: x.group().title(), search_string)

    # capitalize first letter after a hyphen
    search_string = re.sub("(-.)", lambda x: x.group().title(), search_string)

    # Create the query URL
    return search_string


def kagi_linkhandler(message, sum_type, kagi_token):
    if "http" in message.clean_content:
        message_type = "url"
    else:
        message_type = "text"
    params = {
        message_type: message.clean_content,
        "summary_type": sum_type
    }
    headers = {"Authorization": f"Bot {kagi_token}"}
    try:
        output = requests.post("https://kagi.com/api/v0/summarize", headers=headers, params=params)
        kagi_response = output.json()["data"]["output"]
    except:
        kagi_response = "An error occurred in trying to get your summary. One possible cause is that your video " \
                        "might not have a transcript. "
    return f"```{kagi_response}```"


class LinkType(Enum):
    WIKI = 0
    MARKET = 1
