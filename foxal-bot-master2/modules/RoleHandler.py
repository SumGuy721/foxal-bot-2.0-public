from datetime import datetime, timedelta
from enum import Enum

import discord.utils
from discord import Forbidden, DiscordException
from discord.utils import find
from discord.ext.commands import RoleNotFound
# noinspection PyPackages
from . import Sheets
from . import Scheduling as Scheduler


def find_role(desired_role, role_list):
    """
    Finds a role object within a list of roles based on string input
    :param desired_role: string of the role name that needs to be searched for
    :param role_list: list of role objects
    :return: Returns a role object if found, else None
    """
    found_role = find(lambda role: role.name.casefold() == desired_role.casefold(), role_list)
    if found_role:
        return found_role
    else:
        return


def find_role_users(role_name, role_list):
    role = find_role(role_name, role_list)
    usernames = [member.id for member in role.members]
    return usernames


# Addrole and removerole
async def role_update(ctx, action_type, args):
    role_list = ctx.guild.roles
    des_role = find(lambda role: role.name.casefold() == args.casefold(), role_list)
    try:
        if action_type == RoleUpdateType.ADD:
            await ctx.author.add_roles(des_role)
        else:
            await ctx.author.remove_roles(des_role)
    except Forbidden:
        await ctx.send("You or the bot do not have the permissions to add this role to yourself")
    except RoleNotFound:
        await ctx.send("Role was not found")
        print("Role not found in changing members role")


class RoleUpdateType(Enum):
    ADD = 0
    REMOVE = 1


# Initiation start
async def initiator(initiate, notification_role, initiate_role, new_role, check_times, failed_initiate_role):
    try:
        await initiate.add_roles(notification_role)  # should be done in one call since add_roles takes a list
        await initiate.add_roles(initiate_role)
        await initiate.remove_roles(new_role)
        await initiate.remove_roles(failed_initiate_role)
    except DiscordException:
        print("Unexpected error while updating roles")
    today = datetime.today().strftime("%d/%m/%y")
    fast_date = datetime.now() + timedelta(days=check_times[0])
    normal_date = datetime.now() + timedelta(days=check_times[1])
    slow_date = datetime.now() + timedelta(days=check_times[2])
    await Sheets.add_initiate(initiate.display_name, today)
    await Scheduler.add_job(initiate.id, fast_date, "Fast initiation")
    await Scheduler.add_job(initiate.id, normal_date, "Normal initiation")
    await Scheduler.add_job(initiate.id, slow_date, "Slow initiation")


# Initiate > Member
# noinspection PyBroadException
async def initiate_clan_promote(ctx, name, initiate_role_name, clan_member_role_name, member_role_name):
    """
        Promotes initiate named in command to member
        """
    initiate_role = find_role(initiate_role_name, ctx.guild.roles)
    clan_member_role = find_role(clan_member_role_name, ctx.guild.roles)
    member_role = find_role(member_role_name, ctx.guild.roles)
    try:
        member = name
        await member.remove_roles(initiate_role)
        await member.add_roles(clan_member_role)
        await member.add_roles(member_role)
    except:
        print("Unexpected error while updating roles")
        ctx.send("Unexpected error while updating roles")


# unroled > community member
# noinspection PyBroadException
async def initiate_com_promote(author, notif_role, com_member_role, member_role, new_role):
    """
    Promotes unroled named in command to community member
    """
    try:
        await author.add_roles(com_member_role)
        await author.add_roles(member_role)
        await author.add_roles(notif_role)
        await author.remove_roles(new_role)
    except:
        print("Unexpected error while updating roles")


# Check if user has role
def user_has_role(rolename, user_id, bot, server_id):
    try:
        found_role = find_role(rolename, bot.get_guild(server_id).roles)
        all_roles = bot.get_guild(server_id).get_member(user_id).roles
        present = found_role in all_roles
        return present
    except:
        print("Error in trying to find out if someone has a role")
