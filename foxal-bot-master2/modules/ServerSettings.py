import json


# noinspection PyAttributeOutsideInit
class ChannelSettings:

    def __init__(self, file):
        with open(file) as f:
            self.config = json.load(f)
        self.map_data()

    def map_data(self):
        self.SQUAD_FINDER_CHANNEL_ID = self.config['GENERAL']['SquadFinderChannelId']
        self.AWAY_SQUAD_CHANNEL_ID = self.config['GENERAL']['AwaySquadChannelId']
        self.ADMIN_COMMAND_CHANNEL_ID = self.config['GENERAL']['AdminCommandChannelId']
        self.BOT_COMMAND_CHANNEL_ID = self.config['GENERAL']['BotCommandChannelId']
        self.GATEWAY_CHANNEL_ID = self.config['GENERAL']['GatewayChannelId']
        self.MEET_AND_GREET_CHANNEL_ID = self.config['GENERAL']['MeetAndGreetChannelId']
        self.NOTIFICATIONS_CHANNEL_ID = self.config['GENERAL']['NotificationsChannelId']
        self.INITIATOR_CHAT_CHANNEL_ID = self.config['GENERAL']['InitiatorChatChannelId']
        self.BOT_LOG_CHANNEL_ID = self.config['GENERAL']['BotLogChannelId']
        self.SERVER_ID = self.config['GENERAL']['ServerId']
        self.GENERAL_CHANNEL_ID = self.config['GENERAL']['GeneralChannelId']
        self.GUIDE_ROLE = self.config['ROLES']['Guide']
        self.INITIATOR_ROLE = self.config['ROLES']['Initiator']
        self.SQUAD_STARTER_ROLE = self.config['ROLES']['Squad Starter']
        self.SQUAD_JOINER_ROLE = self.config['ROLES']['Squad Joiner']
        self.INACTIVE_LURKER_ROLE = self.config['ROLES']['Inactive Lurker']
        self.FAILED_INITIATE_ROLE = self.config['ROLES']['Failed Initiate']
        self.KNOWN_ABSENCE_ROLE = self.config['ROLES']['Known Absence']
        self.LURK_STREAK_ROLE = self.config['ROLES']['Lurk Streak']
        self.GRAVEYARD_ROLE = self.config['ROLES']['The Graveyard']
        self.INITIATE_ROLE = self.config['ROLES']['Initiate']
        self.NEW_JOINER = self.config['ROLES']['New Joiner']
        self.COMM_MEM_ROLE = self.config['ROLES']['Community Member']
        self.CLAN_MEM_ROLE = self.config['ROLES']['Clan Member']
        self.NOTIFICATIONS = self.config['ROLES']['Notifications']
        self.MEMBER_ROLE = self.config['ROLES']['Member']
        self.ROLEWHITELIST = self.config['ROLEWHITELIST']
        self.REDNAMETHRESHOLD = self.config['REDNAMES']['Redname Thresh']
        self.KATHRESHOLD = self.config['REDNAMES']['KA Thresh']
        self.REDNAMESTREAK = self.config['REDNAMES']['Streak Redname Thresh']
        self.KASTREAK = self.config['REDNAMES']['Streak KA Thresh']
        self.THUMBS_EMOTE = self.config['EMOTES']['Thumbs Up']
        self.WAVE_EMOTE = self.config['EMOTES']['Wave']
        self.HEART_EMOTE = self.config['EMOTES']['Heart']
        self.CLEM_EMOTE = self.config['EMOTES']['Clem']
        self.INIT_THRESHOLDS = self.config['INITIATION']
        self.CHECK_TIMES = self.config['INITIATION_TIMES']
