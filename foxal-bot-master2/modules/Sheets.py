import sys

# Google Sheets stuff
import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = [
    "https://spreadsheets.google.com/feeds",
    "https://www.googleapis.com/auth/spreadsheets",
    "https://www.googleapis.com/auth/drive.file",
    "https://www.googleapis.com/auth/drive"
]
credentials = ServiceAccountCredentials.from_json_keyfile_name("creds.json", scope)
client = gspread.authorize(credentials)


async def authorize():
    return gspread.authorize(credentials)


async def add_initiate(name, date):
    # noinspection PyBroadException
    try:
        gc = await authorize()
        sheet = gc.open("Foxal - Admin sheets").worksheet("Initiation")
        row = sheet.row_values(2, value_render_option="FORMULA")
        sheet.insert_row(row, 2, value_input_option="USER_ENTERED")
        sheet.update_cell(2, 2, date)
        sheet.update_cell(2, 1, name)
        # clear message count and comment fields {"userEnteredFormat": {"numberFormat":
        cells = sheet.range("M2:Z2")
        for cell in cells:
            cell.value = ""
        sheet.update_cells(cells)
    except:
        print("Error adding initiate to admin sheet: ", sys.exc_info()[0], ": ", sys.exc_info()[1])
