import sqlite3

import discord

from .RoleHandler import user_has_role, find_role
from .Initiation import fetch_fail_db
from .MiscFunctions import get_visible_name

# Monthly count of messages
total_con = sqlite3.connect("modules/TotalMessages.db")
total_cur = total_con.cursor()
# History of rednames
red_history_con = sqlite3.connect("modules/RedNames.db")
red_history_cur = red_history_con.cursor()
# Current track of rednames
red_track_con = sqlite3.connect("modules/RedNames.db")
red_track_cur = red_track_con.cursor()


def setup_msg_db():
    total_cur.execute("CREATE TABLE IF NOT EXISTS messages(user_id int, messagecount int)")
    total_con.commit()
    red_history_cur.execute("CREATE TABLE IF NOT EXISTS redname_history(user_id int, lurker_count int, ka_count int)")
    red_history_con.commit()
    red_track_cur.execute("CREATE TABLE IF NOT EXISTS redname_tracker(user_id int, lurker_count int"
                          ", ka_count int)")
    red_track_con.commit()


async def add_msg_count(user_id):
    """
    Adds a message author to the database and sets to/increases count by 1
    user_id = int
    """
    # Check if user is already in db
    check_query = "SELECT user_id FROM messages WHERE user_id = ?"
    # Add to count if yes
    if total_cur.execute(check_query, [user_id]).fetchone():
        total_cur.execute("""UPDATE messages SET messagecount = messagecount + 1
                            WHERE user_id = ?""", [user_id])
        total_con.commit()
    # Create if not
    else:
        total_cur.execute("INSERT INTO messages VALUES (?,?)", [user_id, 1])
        total_con.commit()


async def reduce_ka_count(bot, server_id, user_id, ka_role_name, ka_treshold, bot_log_channel_id, graveyard_role_name):
    """
    Adds a message author to the database and sets to/increases count by 1
    user_id = int
    """
    # Check if user is already in db
    guild = bot.get_guild(server_id)
    check_query = "SELECT user_id FROM redname_tracker WHERE user_id = ?"
    if red_track_cur.execute(check_query, [user_id]).fetchone():
        status = red_track_cur.execute("SELECT ka_count FROM redname_tracker WHERE user_id = ?", [user_id]).fetchone()
        if status[0] == 0:
            red_track_cur.execute("UPDATE redname_tracker SET ka_count = ? WHERE user_id = ?", [ka_treshold, user_id])
            red_track_con.commit()
        else:
            red_track_cur.execute("UPDATE redname_tracker SET ka_count = ka_count - 1 WHERE user_id = ?", [user_id])
            red_track_con.commit()
        if status[0] == 1:
            await guild.get_member(user_id).remove_roles(find_role(ka_role_name, guild.roles))
            await guild.get_member(user_id).remove_roles(find_role(graveyard_role_name, guild.roles))
            channel = guild.get_channel(bot_log_channel_id)
            await channel.send(f"Role {ka_role_name} has been removed from {get_visible_name(bot, server_id, user_id)}")
    else:
        red_track_cur.execute("INSERT INTO redname_tracker VALUES (?,?,?)", [user_id, 0, ka_treshold])
        red_track_con.commit()


async def reduce_lurk_count(bot, server_id, user_id, lurk_role_name, lurk_threshold, bot_log_channel_id,
                            lurk_streak_role_name):
    """
    Adds a message author to the database and sets to/increases count by 1
    user_id = int
    """
    # Check if user is already in db
    guild = bot.get_guild(server_id)
    check_query = "SELECT user_id FROM redname_tracker WHERE user_id = ?"
    if red_track_cur.execute(check_query, [user_id]).fetchone():
        status = red_track_cur.execute("SELECT lurker_count FROM redname_tracker WHERE user_id = ?",
                                       [user_id]).fetchone()
        lurk_min_one = lurk_threshold - 1
        if status[0] == 0:
            red_track_cur.execute("UPDATE redname_tracker "
                                  "SET lurker_count = ?  WHERE user_id = ?", [lurk_min_one, user_id])
            red_track_con.commit()
        else:
            red_track_cur.execute("UPDATE redname_tracker SET lurker_count = lurker_count - 1 WHERE user_id = ?",
                                  [user_id])
            red_track_con.commit()
        if status[0] == 1:
            await guild.get_member(user_id).remove_roles(find_role(lurk_role_name, guild.roles))
            await guild.get_member(user_id).remove_roles(find_role(lurk_streak_role_name, guild.roles))
            channel = guild.get_channel(bot_log_channel_id)
            await channel.send(f"Role {lurk_role_name} has been removed from"
                               f" {get_visible_name(bot, server_id, user_id)}")
    else:
        red_track_cur.execute("INSERT INTO redname_tracker VALUES (?,?,?)", [user_id, lurk_threshold, 0])
        red_track_con.commit()


def get_totalmessage():
    query = "SELECT user_id, messagecount FROM messages ORDER BY messagecount DESC"
    return total_cur.execute(query).fetchall()


def update_values(column, update_type, user_id):
    # Add 1 to column
    if update_type == "ADD":
        red_history_cur.execute(f"""UPDATE redname_history SET {column} = {column} + 1
                            WHERE user_id = ?""", [user_id])
        result = red_history_cur.execute(f"SELECT {column} FROM redname_history WHERE user_id = ?",
                                         [user_id]).fetchone()
        red_history_con.commit()
        return result
    # Set column to 0
    if update_type == "RESET":
        red_history_cur.execute(f"""UPDATE redname_history SET {column} = 0
                            WHERE user_id = ?""", [user_id])
        result = red_history_cur.execute(f"SELECT {column} FROM redname_history WHERE user_id = ?",
                                         [user_id]).fetchone()
        red_history_con.commit()
        return result


def update_red_history(user_id, red_type, update_type):
    """
    Update the db to show the user redname history
    user_id = user to update for
    red_type = 0 (inactive lurker) or 1 (known absence)
    update_type = ADD (increase by 1) or RESET (set to 0)
    """

    # Create if not in db yet
    check_query = "SELECT user_id FROM redname_history WHERE user_id = ?"
    if not red_history_cur.execute(check_query, [user_id]).fetchone():
        red_history_cur.execute("INSERT INTO redname_history VALUES (?,?,?)", [user_id, 0, 0])

    # Set what column to edit and call for function to edit
    if red_type == 0:
        column = "lurker_count"
        result = update_values(column, update_type, user_id)
        return result
    if red_type == 1:
        column = "ka_count"
        result = update_values(column, update_type, user_id)
        return result


# noinspection PyBroadException
async def get_total_results(bot, server_id, ka_role, redname_role_name, redname_threshold, red_rem_thresh,
                            ka_rem_thresh, admin_channel_id, graveyard_role_name, lurk_streak_role_name,
                            initiate_role_name, failed_initiate_role):
    guild = bot.get_guild(server_id)
    total = get_totalmessage()
    channel = bot.get_channel(admin_channel_id)
    redname_role = find_role(redname_role_name, guild.roles)
    lurk_streak_role = find_role(lurk_streak_role_name, guild.roles)
    graveyard_role = find_role(graveyard_role_name, guild.roles)
    usernames = ""
    messagecount = ""
    ka_names = ""
    red_names = ""
    lurk_streak_names = ""
    graveyard_names = ""
    message_list = list()
    failed_initiates = ""

    # Create a list of members in server, later functions removes users that have messaged to leave a list
    # of non-messaging users
    server_members = list()
    for member in guild.members:
        server_members.append(member.id)

    # Members that have messaged during the month
    for id_and_count in total:
        user = bot.get_user(id_and_count[0])
        if bot.get_user(id_and_count[0]) in guild.members and not bot.get_user(id_and_count[0]).bot:
            if not user_has_role(initiate_role_name, id_and_count[0], bot, server_id):
                message_list.append((id_and_count[0], id_and_count[1]))
                server_members.remove(user.id)
                user_has_ka = user_has_role(ka_role, id_and_count[0], bot, server_id)

                # Known Absence
                if user_has_ka:
                    ka_names += get_visible_name(bot, server_id, id_and_count[0]) + "\n"
                    streak = update_red_history(id_and_count[0], 1, "ADD")
                    if streak[0] >= ka_rem_thresh:
                        guild.get_member(id_and_count[0]).add_roles(graveyard_role)
                        update_red_history(id_and_count[0], 1, "RESET")
                        graveyard_names += get_visible_name(bot, server_id, id_and_count[0]) + "\n"

                # No KA
                if id_and_count[1] <= redname_threshold and not user_has_ka:
                    red_names += get_visible_name(bot, server_id, id_and_count[0]) + "\n"
                    await guild.get_member(id_and_count[0]).add_roles(redname_role)
                    streak = update_red_history(id_and_count[0], 0, "ADD")
                    if streak[0] >= red_rem_thresh:
                        await guild.get_member(id_and_count[0]).add_roles(lurk_streak_role)
                        update_red_history(id_and_count[0], 0, "RESET")
                        lurk_streak_names += get_visible_name(bot, server_id, id_and_count[0]) + "\n"

                # Reset the redname history if user has messaged enough
                if id_and_count[1] > redname_threshold:
                    update_red_history(id_and_count[0], 0, "RESET")
                    update_red_history(id_and_count[0], 1, "RESET")

    # Members that have not messeged during the month
    for member_id in server_members:
        if not bot.get_user(member_id).bot:
            # Protecting Smol-high-copocity-looder-with-m from getting rednamed, important stuff
            if not member_id == 470628078096220160:
                user_has_ka = user_has_role(ka_role, member_id, bot, server_id)
                if not user_has_role(initiate_role_name, member_id, bot, server_id):

                    # Known Absence
                    if user_has_ka:
                        message_list.append((member_id, 0))
                        ka_names += get_visible_name(bot, server_id, member_id) + "\n"
                        streak = update_red_history(member_id, 1, "ADD")
                        if streak[0] >= ka_rem_thresh:
                            await guild.get_member(member_id).add_roles(graveyard_role)
                            update_red_history(member_id, 1, "RESET")
                            graveyard_names += get_visible_name(bot, server_id, member_id) + "\n"

                    # No KA; no messages always means redname
                    else:
                        message_list.append((member_id, 0))
                        red_names += get_visible_name(bot, server_id, member_id) + "\n"
                        await guild.get_member(member_id).add_roles(redname_role)
                        streak = update_red_history(member_id, 0, "ADD")
                        if streak[0] >= red_rem_thresh:
                            await guild.get_member(member_id).add_roles(lurk_streak_role)
                            update_red_history(member_id, 0, "RESET")
                            lurk_streak_names += get_visible_name(bot, server_id, member_id) + "\n"

    embed = discord.Embed(title="Messages overview", description="These are the counts as stored in the DB")
    for user in message_list:
        channel = bot.get_channel(admin_channel_id)
        usernames += get_visible_name(bot, server_id, user[0]) + "\n"
        messagecount += str(user[1]) + "\n"
        if len(usernames) >= 992:
            embed.add_field(name="Username", value=usernames)
            embed.add_field(name="Count", value=messagecount)
            await channel.send(embed=embed)
            usernames = ""
            messagecount = ""
    else:
        if len(usernames) > 0:
            embed.add_field(name="Username", value=usernames)
            embed.add_field(name="Count", value=messagecount)
            await channel.send(embed=embed)

    # Failed initiates
    try:
        for id in fetch_fail_db():
            member = guild.get_member(id)
            if member in guild.members:
                member.add_roles(find_role(failed_initiate_role, guild.roles))
                failed_initiates += get_visible_name(bot, server_id, id) + "\n"
    except:
        print("Failed in fetching the failed initiates for the monthly check.")

    # Stuff breaks if a field in the embed is empty
    if len(red_names) <= 0:
        red_names = "None"
    if len(ka_names) <= 0:
        ka_names = "None"
    if len(lurk_streak_names) <= 0:
        lurk_streak_names = "None"
    if len(graveyard_names) <= 0:
        graveyard_names = "None"
    if len(failed_initiates) <= 0:
        failed_initiates = "None"

    try:
        embed = discord.Embed(title="Monthly overview of all messagecounts", description="Red names have automatically "
                                                                                         "been applied")
        # Adding the fields with the values
        embed.add_field(name="RedNamed", value=red_names)
        embed.add_field(name="Known Absence", value=ka_names)
        embed.add_field(name="Lurk Streak", value=lurk_streak_names)
        embed.add_field(name="Graveyard", value=graveyard_names)
        embed.add_field(name="Failed initiates", value=failed_initiates)
        await channel.send(embed=embed)
    except:
        await channel.send("An error occurred in the final bit of sending the rednames")


def clear_total_messages_db():
    total_cur.execute("DELETE FROM messages")


async def get_purge_embed(bot, server_id, admin_channel_id, redname_role_name,
                          lurk_streak_role_name, graveyard_role_name, failed_initiate_role_name):
    embed = discord.Embed(title="Users who still have redname roles", description="The bot has not done anything with "
                                                                                  "regard to these members, "
                                                                                  "only sent this notification")
    guild = bot.get_guild(server_id)
    channel = bot.get_channel(admin_channel_id)
    redname_role = find_role(redname_role_name, guild.roles)
    lurk_streak_role = find_role(lurk_streak_role_name, guild.roles)
    graveyard_role = find_role(graveyard_role_name, guild.roles)
    failed_initiate_role = find_role(failed_initiate_role_name, guild.roles)
    role_list = (redname_role, lurk_streak_role, graveyard_role, failed_initiate_role)
    values = ""
    for role in role_list:
        for user in role.members:
            values += get_visible_name(bot, server_id, user.id) + "\n"
        if len(values) <= 0:
            values = "None"
        embed.add_field(name=role.name, value=values)
        values = ""
    await channel.send(embed=embed)
