# Written in python 3.10
# API wrapper in use is pycord
import json
import random
from datetime import datetime

# noinspection PyPackageRequirements
from discord.ext import tasks

from modules.ServerSettings import ChannelSettings
from modules.RoleHandler import role_update, initiator, find_role, RoleUpdateType, \
    initiate_com_promote, initiate_clan_promote, find_role_users, user_has_role
from modules.LinkHandler import LinkType, link_handler, kagi_linkhandler
from modules.Scheduling import setup_sc_db, get_due_jobs
from modules.Squads import add_sq_count, get_gold_names, setup_sq_db, clear_gold_names_db
from modules.MiscFunctions import get_visible_name, error_message_sender
from modules.TotalMessages import add_msg_count, setup_msg_db, get_total_results, reduce_ka_count, reduce_lurk_count, \
    get_purge_embed, clear_total_messages_db
from modules.Initiation import add_init_count, setup_init_db, get_init_count, init_check_message, remove_init, \
    add_fail_db, clear_fail_db

# Discord stuff
# noinspection PyPackageRequirements
import discord
# noinspection PyPackageRequirements
from discord import Option, option, Member

# Bot instantiation
intents = discord.Intents(guilds=True, members=True, messages=True, reactions=True)
bot = discord.Bot(intents=intents)

# Variables
config = ChannelSettings("config.json")
server_id = config.SERVER_ID
role_whitelist = list(config.ROLEWHITELIST.values())

with open("TOKEN.json") as f:
    data = json.load(f)
    TOKEN = data["TOKEN"]
    kagi_token = data["KAGI_TOKEN"]


# Bot ready message
@bot.event
async def on_ready():
    await bot.wait_until_ready()
    channel = bot.get_channel(config.BOT_LOG_CHANNEL_ID)
    print(f"{bot.user} is ready and online")
    setup_sc_db()
    setup_sq_db()
    setup_msg_db()
    setup_init_db()
    schedule_starter.start()
    guild_members = random.sample(find_role_users(config.MEMBER_ROLE, bot.get_guild(server_id).roles), 2)
    await bot.change_presence(activity=discord.Game(name=f"with {get_visible_name(bot, server_id, guild_members[0])}"
                                                         f" and "
                                                         f"{get_visible_name(bot, server_id, guild_members[1])}"))
    await channel.send(f"{bot.user} is ready and online")


# Slash commands
#   Test command
# @bot.slash_command(name="test", description="This is the command used for testing")
# async def testing(ctx):
#     # await ctx.defer()
#
#     class MyModal(discord.ui.Modal):
#         def __init__(self, *args, **kwargs) -> None:
#             super().__init__(*args, **kwargs)
#
#             self.add_item(discord.ui.InputText(label="Short Input"))
#             self.add_item(discord.ui.InputText(label="Long Input", style=discord.InputTextStyle.long))
#
#         async def callback(self, interaction: discord.Interaction):
#             embed = discord.Embed(title="Modal Results")
#             embed.add_field(name="Short Input", value=self.children[0].value)
#             embed.add_field(name="Long Input", value=self.children[1].value)
#             await interaction.response.send_message(embeds=[embed])
#
#     """Shows an example of a modal dialog being invoked from a slash command."""
#     modal = MyModal(title="Modal via Slash Command")
#     await ctx.send_modal(modal)
    # await ctx.respond("Boop")


# @bot.message_command(name="Test")
# async def beep(ctx, message: discord.MessageInteraction.data):
#     await ctx.respond(message.clean_content)


@bot.message_command(name="Summary")
async def link_summary(ctx, message: discord.MessageInteraction.data):
    await ctx.defer()
    kagi_response = kagi_linkhandler(message, "summary", kagi_token)
    await ctx.respond(kagi_response)


@bot.message_command(name="Keypoints")
async def link_summary(ctx, message: discord.MessageInteraction.data):
    await ctx.defer()
    kagi_response = kagi_linkhandler(message, "takeaway", kagi_token)
    await ctx.respond(kagi_response)


# #   Giveaways
# @bot.slash_command(name="giveaway", description="Start one more more giveaways")
# @option("time", description="Set the time in full days before a winner is chosen", input_type=int)
# @option("minimum_role", description="Choose the minimum rank a user must hold to win",
#         choices=["No minimum", "Initiate", "Member", "In-game member"], input_type=str)
# @option("max_wins_per_entry", description="Set how many items a member can win in this giveaway", input_type=int)
# @option("items_and_rewards", description="Set the giveaways in the format of "
#                                          "reward,giveaway name.reward,giveaway name.etc.", input_type=str)
# async def giveaway_start(ctx, time: int, minimum_role: str, max_wins_per_entry: int, items_and_rewards: str):
#     await ctx.defer()
#     await ctx.respond("a")


#   Lookups
@bot.slash_command(name="wiki", description="Search for an item on the warframe wiki")
@option("item", description="What do you want to search for?")
async def wiki_link(ctx, item: str):
    await link_handler(ctx, link_type=LinkType.WIKI, args=item)


@bot.slash_command(name="wfm", description="Search for an item on warframe.market")
@option("item", description="What do you want to search for?")
async def wfm_link(ctx, item: str):
    await link_handler(ctx, link_type=LinkType.MARKET, args=item)


#   Squadding
@bot.slash_command(name="squad", description="Create a squad for others to join")
@option("goal", description="What do you want to create a squad for?")
async def squad_creation(ctx, args):
    await ctx.defer()
    react_message = await ctx.respond(f"{ctx.author.mention} created a squad for {args}")
    await react_message.add_reaction("▶")
    await add_sq_count(ctx.author.id, "Create")
    add_init_count(ctx.author.id, "Squad", bot, server_id, config.INITIATE_ROLE)


#   Role handling
@bot.slash_command(name="initiate", description="Start your initiation")
@option(name="in_game_name", description="What is the exact name you use in-game?")
async def initiate(ctx, in_game_name):
    notification_role = find_role(config.NOTIFICATIONS, ctx.guild.roles)
    initiate_role = find_role(config.INITIATE_ROLE, ctx.guild.roles)
    new_role = find_role(config.NEW_JOINER, ctx.guild.roles)
    failed_initiate_role = find_role(config.FAILED_INITIATE_ROLE, ctx.guild.roles)
    author = ctx.author
    general_channel = ctx.guild.get_channel(config.GENERAL_CHANNEL_ID)
    await author.edit(nick=in_game_name)
    visible_name = get_visible_name(bot, server_id, author.id)
    if user_has_role(config.INITIATE_ROLE, author.id, bot, server_id):
        ctx.respond(f"Hello {visible_name}, you have restarted your initiation. {config.THUMBS_EMOTE}"
                    f"\nYour next checks will be in 7 and 14 days.", ephemeral=True)
        try:
            await initiator(author, notification_role, initiate_role, new_role, list(config.CHECK_TIMES.values()),
                            config.FAILED_INITIATE_ROLE)
        except:
            await error_message_sender(bot, config.BOT_LOG_CHANNEL_ID, "Adding an initiate to the database")
    else:
        await ctx.respond(f">>> Welcome, {visible_name}. Your initiation has been started. {config.THUMBS_EMOTE}"
                          f"\nYou are able to do all the same "
                          f"things as a clan member, just without being in the in-game clan."
                          f"\n\nA couple of small tips: \nWhen you create squads using **#squad-finder**, "
                          f"your initiation "
                          f"will progress a bit faster. \nFor the server rules, take a peek at #server-overview. \nIf "
                          f"you "
                          f"would like some help with finding your way around the game, take a look at "
                          f"**#basic-guides** or "
                          f"ask away in **#helping-hand**. \nLastly, to start chatting with our other members, "
                          f"head into **#general**. \n\nWelcome to our clan! {config.CLEM_EMOTE}", ephemeral=True)
        await initiator(author, notification_role, initiate_role, new_role, list(config.CHECK_TIMES.values()),
                        failed_initiate_role)
        await general_channel.send(f"{author.mention} has joined the server as an Initiate, say hello!")


@bot.slash_command(name="game_member", description="Make someone a clan member")
async def initiate_clan_promoter(ctx, name: Member):
    await initiate_clan_promote(ctx, name, config.INITIATE_ROLE, config.CLAN_MEM_ROLE, config.MEMBER_ROLE)
    await ctx.respond(f"Roles have been replaced on {name.name}")
    remove_init(name.id)


@bot.slash_command(name="community_member", description="Make someone a community member")
@option(name="in_game_name", description="What is the exact name you use in-game?")
async def initiate_com_promoter(ctx, in_game_name):
    notification_role = find_role(config.NOTIFICATIONS, ctx.guild.roles)
    comm_role = find_role(config.COMM_MEM_ROLE, ctx.guild.roles)
    member_role = find_role(config.MEMBER_ROLE, ctx.guild.roles)
    new_role = find_role(config.NEW_JOINER, ctx.guild.roles)
    author = ctx.author
    general_channel = ctx.guild.get_channel(config.GENERAL_CHANNEL_ID)
    await author.edit(nick=in_game_name)
    visible_name = get_visible_name(bot, server_id, author.id)
    await ctx.respond(f">>> Welcome, {visible_name}. You have joined the server as"
                      f" a Community Member. {config.THUMBS_EMOTE}"
                      f"\nYou are able to do all the same things as a clan member, just without being in the "
                      f"in-game clan.\n\nA couple of small tips: \nFor the server rules, take a peek at "
                      f"#server-overview. \nIf you "
                      f"would like some help with finding your way around the game, take a look at "
                      f"**#basic-guides** or "
                      f"ask away in **#helping-hand**. \nLastly, to start chatting with our other members, "
                      f"head into **#general**. \n\nWelcome to our clan! {config.CLEM_EMOTE}", ephemeral=True)
    await initiate_com_promote(author, notification_role, comm_role, member_role, new_role)
    await general_channel.send(f"{author.mention} has joined the server as a Community Member, say hello!")


@bot.slash_command(name="role_change", description="Add or remove a role from yourself")
async def change_role(ctx, role: Option(str, "Choose what role to add", choices=role_whitelist[0:])):
    add_or_remove = "Error"
    msg = "Error"
    if find_role(role, ctx.guild.roles) not in ctx.author.roles:
        add_or_remove = RoleUpdateType.ADD
        msg = f"Added {role} to {ctx.author.mention}"
    elif find_role(role, ctx.guild.roles) in ctx.author.roles:
        add_or_remove = RoleUpdateType.REMOVE
        msg = f"Removed {role} from {ctx.author.mention}"
    else:
        await ctx.respond("Error in changing your role")
        print("Role was not found on user nor was it found")
    try:
        await role_update(ctx, add_or_remove, role)
        await ctx.respond(msg)
    except:
        await ctx.respond("An error occurred in changing your role")
        print(f"Error in changing {ctx.author.name}'s role")


# Bot handling
@bot.slash_command(name="shutdown", description="Disconnect the bot from Discord, panic button")
async def shutdown(ctx):
    """
    Shuts down the bot in case of emergencies (endless send loop or something like that)
    """
    if ctx.channel.id == config.ADMIN_COMMAND_CHANNEL_ID:
        exit("Bot shut down as requested.")


@bot.slash_command(name="privacy", description="Statement on what the data collected is used for")
async def privacy(ctx):
    privacy_message = "https://discord.com/channels/397407276304039937/597473409856831495/597487368181907476"
    await ctx.respond(f"For what data is being stored, view the bottom of the following message: {privacy_message}")


@bot.slash_command(name="pong", description="Sends the bots latency.")
async def pong(ctx):
    await ctx.respond(f"Pong. Latency is {bot.latency}")


# Listener actions
# On message actions
@bot.listen()
async def on_message(message):
    author_id = message.author.id
    # Add message count
    await add_msg_count(author_id)
    if message.author != bot.user:
        if user_has_role(config.KNOWN_ABSENCE_ROLE, author_id, bot, server_id):
            await reduce_ka_count(bot, server_id, author_id, config.KNOWN_ABSENCE_ROLE,
                                  config.KATHRESHOLD, config.BOT_LOG_CHANNEL_ID, config.GRAVEYARD_ROLE)
        if user_has_role(config.INACTIVE_LURKER_ROLE, author_id, bot, server_id):
            await reduce_lurk_count(bot, server_id, author_id, config.INACTIVE_LURKER_ROLE,
                                    config.REDNAMETHRESHOLD, config.BOT_LOG_CHANNEL_ID, config.LURK_STREAK_ROLE)
        add_init_count(author_id, "Message", bot, server_id, config.INITIATE_ROLE, channel=message.channel,
                       meet_channel_id=config.MEET_AND_GREET_CHANNEL_ID)


#   Squadding reaction listener
@bot.listen()
async def on_reaction_add(react, user):
    channel = react.message.channel
    channel_id = react.message.channel.id
    bot_is_not_author = str(user) != str(bot.user)
    bot_created_reaction = react.message.author == bot.user
    if bot_is_not_author and react.emoji == '▶' and bot_created_reaction:
        if channel_id == config.SQUAD_FINDER_CHANNEL_ID or channel_id == config.AWAY_SQUAD_CHANNEL_ID:
            await channel.send(f"{user.mention} has joined the squad", reference=react.message)
        await add_sq_count(user.id, "Join")
    add_init_count(user.id, "Squad", bot, server_id, config.INITIATE_ROLE)


@bot.listen()
async def on_reaction_remove(react, user):
    channel = react.message.channel
    channel_id = react.message.channel.id
    bot_is_not_author = str(user) != str(bot.user)
    bot_created_reaction = react.message.author == bot.user
    if bot_is_not_author and react.emoji == '▶' and bot_created_reaction:
        if channel_id == config.SQUAD_FINDER_CHANNEL_ID or channel_id == config.AWAY_SQUAD_CHANNEL_ID:
            await channel.send(f"{user.mention} has left the squad", reference=react.message)


#   Joins and leaves
@bot.listen()
async def on_member_join(member):
    role_list = bot.get_guild(server_id).roles
    guide_role = find_role(config.GUIDE_ROLE, role_list)
    trusted_role = find_role(config.INITIATOR_ROLE, role_list)
    new_role = find_role(config.NEW_JOINER, role_list)
    message_string = 'Hello there {user}, welcome to Foxal! {wave}\n\n' \
                     'This message is assuming that you are here to join the in-game clan, ' \
                     ' if you are just here for the discord community you can skip ahead to ' \
                     'the /community_member command' \
                     '\n\nJust so you are aware, we have a small initiation phase where we make sure that the people ' \
                     'who join are also active in the community. ' \
                     '\nIt\'s basically just there to stop people from hopping in, ' \
                     'nabbing some blueprints and immediately leaving again. ' \
                     '\nUsually this lasts about a week, and it is based on ' \
                     'discord messages and squads created, nothing more.' \
                     '\n\nIf this sounds good to you, please use the command **/initiate** to start it.' \
                     '\nIf you are just here for the community, please use **/community_member**. ' \
                     '\nThis will give you full access to the discord, ' \
                     'but will skip the initiation as described above.' \
                     '\n\nIf you have any questions, ' \
                     'please do ask a {guide} or {trusted} (in this chat, not DM\'s) {heart}'
    formatted_string = message_string.format(user=member.mention, wave=config.WAVE_EMOTE, guide=guide_role.mention,
                                             trusted=trusted_role.mention, heart=config.HEART_EMOTE)
    await bot.get_channel(config.MEET_AND_GREET_CHANNEL_ID).send(formatted_string)
    await member.add_roles(new_role)


@bot.listen()
async def on_member_remove(member):
    channel = bot.get_channel(config.GATEWAY_CHANNEL_ID)
    nickname = str(member.nick)
    if member.joined_at:
        formatted_date = member.joined_at.strftime("%a, %d %b. %Y %H:%M UTC")
    else:
        formatted_date = "Unknown"
    string_format = f"Name: **{member.name}**\nNickname: **{nickname}**\nJoined at: **{formatted_date}**\nhas " \
                    f"decided to leave. "
    await channel.send(string_format)


# Loops for the scheduler
@tasks.loop(minutes=1.0)
async def schedule_starter():
    if datetime.utcnow().strftime("%H") == "12":
        schedule_checker.start()
        schedule_starter.stop()


# noinspection PyBroadException
@tasks.loop(hours=24)
async def schedule_checker():
    init_channel = bot.get_channel(config.INITIATOR_CHAT_CHANNEL_ID)
    bot_channel = bot.get_channel(config.BOT_LOG_CHANNEL_ID)
    job = get_due_jobs()
    guild = bot.get_guild(server_id)
    for x in job:
        try:
            user = guild.get_member(x[1])
            msg_counts = get_init_count(x[1])
            if user in guild.members:
                visible_name = get_visible_name(bot, server_id, user.id)
                if msg_counts:
                    init_score = msg_counts[0] + list(config.INIT_THRESHOLDS.values())[2] * msg_counts[1]
                    if x[2] == "Fast initiation":
                        if init_score >= list(config.INIT_THRESHOLDS.values())[1]:
                            await init_check_message(visible_name, init_channel, msg_counts, bot_channel, "Fast",
                                                     "Pass")
                        else:
                            await init_check_message(visible_name, init_channel, msg_counts, bot_channel, "Fast",
                                                     "Fail")
                    elif x[2] == "Normal initiation":
                        if init_score >= list(config.INIT_THRESHOLDS.values())[0]:
                            await init_check_message(visible_name, init_channel, msg_counts, bot_channel, "Normal",
                                                     "Pass")
                        else:
                            await init_check_message(visible_name, init_channel, msg_counts, bot_channel, "Normal",
                                                     "Fail")
                    elif x[2] == "Slow initiation":
                        if init_score >= list(config.INIT_THRESHOLDS.values())[0]:
                            await init_check_message(visible_name, init_channel, msg_counts, bot_channel, "Slow",
                                                     "Pass")
                        else:
                            await init_check_message(visible_name, init_channel, msg_counts, bot_channel, "Slow",
                                                     "Fail")
                            await user.add_roles(find_role(config.NEW_JOINER, guild.roles))
                            add_fail_db(x[1])
                    else:
                        print(f"Unknown job found in schedule checker: {x[2]}")
                else:
                    if user_has_role(config.INITIATE_ROLE, user.id, bot, server_id):
                        await bot.get_channel(config.INITIATOR_CHAT_CHANNEL_ID).send(
                            f"{visible_name} has either not sent any messages, or is somehow not in the database."
                        )
            else:
                left_user = await bot.fetch_user(x[1])
                await bot.get_channel(config.INITIATOR_CHAT_CHANNEL_ID).send(
                    f"{left_user.name} has left the server before this check."
                )
                remove_init(user.id)
        except:
            await error_message_sender(bot, config.BOT_LOG_CHANNEL_ID, "Checking initiation jobs")

    try:
        guild_members = random.sample(find_role_users(config.MEMBER_ROLE, bot.get_guild(server_id).roles), 2)
        await bot.change_presence(activity=discord.Game(name=f"with "
                                                             f"{get_visible_name(bot, server_id, guild_members[0])}"
                                                             f" and "
                                                             f"{get_visible_name(bot, server_id, guild_members[1])}"))
    except:
        await error_message_sender(bot, config.BOT_LOG_CHANNEL_ID, "Changing the bot status message")
    today = datetime.today().strftime("%d")
    if today in ["01", "11", "21", "23", "28"]:
        if today in ["01", "11", "21"]:
            await bot.get_channel(config.ADMIN_COMMAND_CHANNEL_ID).send(
                "Reminder for the recruit post (reminders are on the 1st; 11th and 21st)"
            )
        if today == "01":
            try:
                clear_total_messages_db()
                role_list = bot.get_guild(server_id).roles
                # Removing from starters
                starter_role = find_role(config.SQUAD_STARTER_ROLE, role_list)
                starter_users = find_role_users(config.SQUAD_STARTER_ROLE, role_list)
                for x in starter_users:
                    member = guild.get_member(x)
                    await member.remove_roles(starter_role)
                # Removing from joiners
                joiner_role = find_role(config.SQUAD_JOINER_ROLE, role_list)
                joiner_users = find_role_users(config.SQUAD_JOINER_ROLE, role_list)
                for x in joiner_users:
                    member = guild.get_member(x)
                    await member.remove_roles(joiner_role)
                # Adding new roles
                starter_mentions = "Starters"
                joiner_mentions = "Joiners"
                new_gold_names = get_gold_names()
                if new_gold_names[0] is not None:
                    for user_id in new_gold_names[0]:
                        new_starter = guild.get_member(user_id)
                        await new_starter.add_roles(starter_role)
                        starter_mentions += f"; {new_starter.mention}"
                if new_gold_names[1] is not None:
                    print(new_gold_names[1])
                    for user_id in new_gold_names[1]:
                        new_joiner = guild.get_member(user_id)
                        await new_joiner.add_roles(joiner_role)
                        joiner_mentions += f"; {new_joiner.mention}"
                # Creating notifications message
                if starter_mentions == "Starters":
                    starter_mentions = ""
                if joiner_mentions == "Joiners":
                    joiner_mentions = ""
                await bot.get_channel(config.NOTIFICATIONS_CHANNEL_ID).send(
                    f"""This month's most active squad starters and joiners are:
                    \n {starter_mentions} 
                    \n {joiner_mentions}"""
                )
            except:
                await bot.get_channel(config.BOT_LOG_CHANNEL_ID).send(
                    "Error in creating a notification for the gold roles"
                )
            clear_gold_names_db()
        if today in ["23"]:
            await get_total_results(bot, server_id, config.KNOWN_ABSENCE_ROLE, config.INACTIVE_LURKER_ROLE,
                                    config.REDNAMETHRESHOLD, config.REDNAMESTREAK, config.KASTREAK,
                                    config.ADMIN_COMMAND_CHANNEL_ID, config.GRAVEYARD_ROLE, config.LURK_STREAK_ROLE,
                                    config.INITIATE_ROLE, config.FAILED_INITIATE_ROLE)
            clear_fail_db()
        if today in ["28"]:
            await get_purge_embed(bot, server_id, config.ADMIN_COMMAND_CHANNEL_ID, config.INACTIVE_LURKER_ROLE,
                                  config.LURK_STREAK_ROLE, config.GRAVEYARD_ROLE, config.FAILED_INITIATE_ROLE)


# End
bot.run(TOKEN)
